#define FUSE_USE_VERSION 31


#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/stat.h>
#include <ctype.h>
#include <sys/types.h>
#include <limits.h>
#include <errno.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <libgen.h>
/*
Library yang diperlukan untuk FUSE, manipulasi string,
manajemen direktori, enkripsi base64, dan operasi pada berkas.
*/


#ifndef S_IFREG // Konstanta variabel.
#define S_IFREG 0100000 // Regular file bit mask (octal representation)
#endif


#define MAX_PATH 4096 // Konstanta variabel.


#define MAX_PASSWORD_LENGTH 100 // Konstanta variabel.


char PASSWORD[MAX_PASSWORD_LENGTH] = "mypass";
// PASSWORD untuk menyimpan kata sandi.




char SOURCE_DIR[MAX_PATH];
char MOUNT_POINT[MAX_PATH];
// path direktori sumber dan titik mount untuk file sistem.


static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";


char* base64_encode(const unsigned char* data, size_t inputLen) {
    // Fungsi ini digunakan untuk mengenkripsi string menggunakan algoritma base64.


    // Fungsi ini menerima argumen data (string yang akan dienkripsi)
    // dan inputLen (panjang data) dan mengembalikan hasil enkripsi.
    printf("Former Name: %s\n", data);
    size_t output_length = 4 * ((inputLen + 2) / 3);
    char* hasilEncode = (char*)malloc(output_length + 1);


    size_t i, j;
    for (i = 0, j = 0; i < inputLen; i += 3, j += 4) {
        unsigned char octet_a = i < inputLen ? data[i] : 0;
        unsigned char octet_b = i + 1 < inputLen ? data[i + 1] : 0;
        unsigned char octet_c = i + 2 < inputLen ? data[i + 2] : 0;


        hasilEncode[j] = base64_table[octet_a >> 2];
        hasilEncode[j + 1] = base64_table[((octet_a & 3) << 4) | (octet_b >> 4)];
        hasilEncode[j + 2] = base64_table[((octet_b & 15) << 2) | (octet_c >> 6)];
        hasilEncode[j + 3] = base64_table[octet_c & 63];
    }


    if (inputLen % 3 == 1) {
        hasilEncode[output_length - 2] = '=';
        hasilEncode[output_length - 1] = '=';
    } else if (inputLen % 3 == 2) {
        hasilEncode[output_length - 1] = '=';
    }


    hasilEncode[output_length] = '\0';
    printf("Encoded Name: %s\n", hasilEncode);
    return hasilEncode;
}


static  int  xmp_getattr(const char *path, struct stat *stbuf){
    // Mengembalikan informasi atribut file dengan memanggil fungsi lstat pada path yang diberikan.
    int res;
    char fpath[MAX_PATH*2];
    // char array sebesar MAX_PATH dikali 2.
    // fpath akan digunakan untuk menyimpan path absolut ke file atau direktori yang akan diperiksa atributnya.


    sprintf(fpath,"%s%s",SOURCE_DIR,path);
    // Menggabungkan SOURCE_DIR (direktori sumber) dengan path yang diberikan sebagai argumen ke dalam fpath.
    // Hal ini akan menghasilkan path absolut menuju file atau direktori yang diminta.


    res = lstat(fpath, stbuf);


    if (res == -1) return -errno;
    /*
    Memeriksa apakah pemanggilan lstat menghasilkan error.
    Jika res bernilai -1, maka pemanggilan lstat gagal,
    dan fungsi akan mengembalikan nilai negatif dari errno
    yang sesuai dengan jenis error yang terjadi.
    */


    return 0;
}


static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    // untuk membaca isi dari sebuah direktori yang diidentifikasi oleh path.
    DIR *dp;
    struct dirent *de;
    /*
    Mendeklarasikan pointer dp untuk menunjukkan ke objek direktori
    yang akan dibaca, dan pointer de untuk menyimpan entri direktori
    saat ini selama iterasi.
    */
    char fpath[MAX_PATH]; // Mendeklarasikan array karakter fpath dengan ukuran MAX_PATH
    snprintf(fpath, sizeof(fpath), "%s%s", SOURCE_DIR, path); // untuk menggabungkan SOURCE_DIR (direktori sumber) dengan path yang diberikan ke dalam fpath.
    (void) offset;
    (void) fi;
    // Mengabaikan parameter offset dan fi untuk menghilangkan peringatan "unused variable" saat kompilasi.
    dp = opendir(fpath);
    if (dp == NULL) return -errno; // Jika operasi ini gagal (dp == NULL), maka fungsi akan mengembalikan nilai negatif dari errno.


    while ((de = readdir(dp)) != NULL) {
        //Memulai loop while untuk membaca entri direktori satu per satu menggunakan fungsi readdir hingga tidak ada entri lagi.
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
            continue;
            // Melakukan pengecekan apakah nama entri saat ini adalah "."
            // (direktori itu sendiri) atau ".." (direktori induk).
            // Jika ya, maka loop akan melanjutkan ke entri direktori berikutnya.


        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;


        char filter_name[MAX_PATH];
        memset(filter_name, 0, sizeof(filter_name));


        strcpy(filter_name, de->d_name);


        // kondisi LUTH
        // jika karakter tersebut adalah 'L', 'U', 'T', atau 'H' baik dalam huruf besar maupun huruf kecil,
        // maka nama filter_name akan diubah
        if (filter_name[0] == 'L' || filter_name[0] == 'l' ||
            filter_name[0] == 'U' || filter_name[0] == 'u' ||
            filter_name[0] == 'T' || filter_name[0] == 't' ||
            filter_name[0] == 'H' || filter_name[0] == 'h') {
           
            char* encoded = base64_encode((unsigned char*)filter_name, strlen(filter_name));
            snprintf(filter_name, sizeof(filter_name), "%s", encoded);
            printf("Convert Name: %s\n", filter_name);
            free(encoded);
        }
        /*
        Hal ini dilakukan dengan menggunakan fungsi base64_encode yang tidak terlihat
        dalam kode yang diberikan. Setelah itu, nama filter_name dicetak dengan
        menggunakan printf.
        */


        // kondisi < 5 ch
        if (strlen(filter_name) < 5) {
            printf("Less than 5: %s\n", filter_name);
            // maka setiap karakter dalam filter_name akan dikonversi menjadi representasi biner dengan 8 bit.


            char new_conv[MAX_PATH];
            memset(new_conv, 0, sizeof(new_conv));
            // Karakter-karakter biner ini akan digabungkan ke dalam new_conv, dan
            // filter_name akan diubah menjadi isi dari new_conv yang baru.
            // Setelah itu, nama filter_name dicetak dengan menggunakan printf.
            for (size_t i = 0; i < strlen(filter_name); i++) {
                char binary[10];
                binary[8] = ' ';
                binary[9] = '\0';


                unsigned char ch = filter_name[i];
                for (int j = 7; j >= 0; j--) {
                    binary[j] = (ch & 1) + '0';
                    ch >>= 1;
                }


                strcat(new_conv, binary);
            }
            memset(filter_name, 0, sizeof(filter_name));
            strcpy(filter_name, new_conv);
            printf("After convert 5: %s\n\n", filter_name);
        }


        if (S_ISREG(st.st_mode)) {
            //Memeriksa apakah entri saat ini adalah sebuah file reguler atau direktori.
            for (int i = 0; filter_name[i]; i++) { // Jika file reguler
                filter_name[i] = tolower(filter_name[i]);
                // maka nama filter_name akan diubah menjadi huruf kecil menggunakan loop for dan fungsi tolower().
            }


            char old_dir_path[MAX_PATH*2];
            snprintf(old_dir_path, sizeof(old_dir_path), "%s/%s", fpath, de->d_name);


            char new_dir_path[MAX_PATH*2];
            snprintf(new_dir_path, sizeof(new_dir_path), "%s/%s", fpath, filter_name);
            // dibentuk path absolut lama (old_dir_path) dan path absolut baru (new_dir_path) menggunakan snprintf
            rename(old_dir_path, new_dir_path);
            // entri direktori dengan path lama akan diubah menjadi path baru dengan nama yang telah diubah.
        }
        else{
            /*
            Jika entri saat ini adalah direktori, maka nama filter_name akan diubah
            menjadi huruf besar menggunakan loop for dan fungsi toupper().
            Proses rename yang sama juga dilakukan untuk direktori.
            */
            for (int i = 0; filter_name[i]; i++) {
                filter_name[i] = toupper(filter_name[i]);
            }


            char old_dir_path[MAX_PATH*2];
            snprintf(old_dir_path, sizeof(old_dir_path), "%s/%s", fpath, de->d_name);


            char new_dir_path[MAX_PATH*2];
            snprintf(new_dir_path, sizeof(new_dir_path), "%s/%s", fpath, filter_name);


            rename(old_dir_path, new_dir_path);
        }


        if (S_ISDIR(st.st_mode)) {


            char new_path[MAX_PATH*2];
            snprintf(new_path, sizeof(new_path), "%s/%s", path, filter_name);
            xmp_readdir(new_path, buf, filler, offset, fi); // pemanggilan rekursif ke fungsi xmp_readdir
        }


        filler(buf, filter_name, &st, 0);
        /*
        filter_name merupakan nama entri yang telah diubah,
        &st adalah pointer ke struktur st yang berisi atribut entri tersebut, dan
        0 adalah offset.
        */
    }


    closedir(dp); // Menutup direktori
    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi) {
    (void) fi;

    if (strcmp(path, "/") == 0) {
        path = SOURCE_DIR;
    } else {
        char fpath[MAX_PATH*2];
        sprintf(fpath, "%s%s", SOURCE_DIR, path);

        struct stat st;
        if (lstat(fpath, &st) == 0 && S_ISREG(st.st_mode)) {
            char inPW[100];
            printf("Enter your Password: ");
            fflush(stdout);
            fgets(inPW, sizeof(inPW), stdin);

            int len = strlen(inPW);
            if (len > 0) inPW[len - 1] = '\0';

            while (strcmp(inPW, PASSWORD) != 0) {
                printf("The password you just entered is incorrect! Please try again.\n\nEnter your Password: ");
                fflush(stdout);
                fgets(inPW, sizeof(inPW), stdin);

                len = strlen(inPW);
                if (len > 0) inPW[len - 1] = '\0';
            }
        }
    }

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char fpath[MAX_PATH*2];
    // Mendeklarasikan array karakter fpath dengan ukuran MAX_PATH*2 untuk menyimpan path absolut dari file yang akan dibaca.
    if(strcmp(path,"/") == 0){ // Memeriksa apakah path adalah root direktori ("/").
        path=SOURCE_DIR; // diubah menjadi SOURCE_DIR (direktori sumber))
        sprintf(fpath,"%s",path); // untuk mengisi fpath dengan nilai path.
    }
    else sprintf(fpath, "%s%s",SOURCE_DIR,path);
    // Jika tidak, maka sprintf digunakan untuk menggabungkan SOURCE_DIR dan path ke dalam fpath.


    int res = 0;
    int fileDIr = 0 ;


    (void) fi; // Mengabaikan parameter fi untuk menghilangkan peringatan "unused variable" saat kompilasi.


    fileDIr = open(fpath, O_RDONLY); // Membuka file dengan menggunakan open dan mode O_RDONLY (hanya membaca).


    if (fileDIr == -1) return -errno; // Jika operasi ini gagal, maka fungsi akan mengembalikan nilai negatif dari errno.


    res = pread(fileDIr, buf, size, offset);
    // Membaca isi file dengan menggunakan file descriptor fileDIr.
    // Data yang dibaca akan disimpan dalam buf dengan ukuran size
    // dan akan dimulai dari offset offset.
   
    // Nilai res akan berisi jumlah byte yang berhasil dibaca.


    if (res == -1) res = -errno; // Memeriksa apakah res bernilai -1
    // jika iya, maka fungsi akan mengembalikan nilai negatif dari errno.


    close(fileDIr);


    return res;
}


static int xmp_mkdir(const char *path, mode_t mode){
    int res; // untuk menyimpan hasil dari pemanggilan sistem.
    char fpath[MAX_PATH]; // untuk menyimpan path absolut dari direktori yang akan dibuat.
    snprintf(fpath, sizeof(fpath), "%s%s", SOURCE_DIR, path);
    // Menggabungkan SOURCE_DIR (direktori sumber) dengan path ke dalam fpath menggunakan snprintf.
    // Ini menghasilkan path absolut ke direktori yang akan dibuat.
    res = mkdir(fpath, mode); // Membuat direktori menggunakan mkdir dengan path fpath dan mode mode.
    // Nilai res akan berisi 0 jika direktori berhasil dibuat, atau -1 jika terjadi kesalahan.
    if (res == -1)
        return -errno;
    return 0;
}


static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    int fileDIr; //  untuk menyimpan file descriptor dari file yang akan ditulis,
    int res; // untuk menyimpan hasil dari pemanggilan sistem.
    char fpath[MAX_PATH];
    snprintf(fpath, sizeof(fpath), "%s%s", SOURCE_DIR, path);
    // Menggabungkan SOURCE_DIR (direktori sumber) dengan path ke dalam fpath menggunakan snprintf.
    // Ini menghasilkan path absolut ke file yang akan ditulis.
    (void)fi; // Mengabaikan parameter fi untuk menghilangkan peringatan "unused variable" saat kompilasi.
    fileDIr = open(fpath, O_WRONLY);
    // Membuka file dengan menggunakan open dan mode O_WRONLY (hanya menulis).
    if (fileDIr == -1)
    // Jika operasi ini gagal (fileDIr == -1), maka fungsi akan mengembalikan nilai negatif dari errno.
        return -errno;


    res = pwrite(fileDIr, buf, size, offset);
    // Menulis ke file dengan menggunakan file descriptor.
    if (res == -1)
        res = -errno;
        // Memeriksa apakah res bernilai -1. Jika ya,
        // maka terjadi kesalahan saat menulis ke file dan res akan diubah menjadi nilai negatif dari errno.


    close(fileDIr);
    return res; // Mengembalikan nilai res
}


static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    // Menetapkan fungsi xmp_getattr sebagai implementasi untuk operasi FUSE getattr.
    // Fungsi ini akan dipanggil ketika isi dari sebuah direktori diminta.
    .readdir = xmp_readdir,
    // Fungsi ini akan dipanggil ketika isi dari sebuah direktori diminta.
    .read = xmp_read,
    // Fungsi ini akan dipanggil ketika isi dari sebuah file dibaca.
    .mkdir = xmp_mkdir, // add new
    // Fungsi ini akan dipanggil ketika direktori baru akan dibuat.
    .write = xmp_write, // add new
    // Fungsi ini akan dipanggil ketika direktori baru akan dibuat.
};




int main(int  argc, char *argv[]) {
  mkdir("mod", 0777); // Membuat direktori "mod" dengan izin (permission) 0777.
  pid_t child_id;
  // Mendeklarasikan variabel child_id bertipe pid_t untuk menyimpan ID proses child.
  child_id = fork(); // Membuat proses child


  if (child_id < 0) { // kurang dari 0, maka proses fork gagal dan exit.
    exit(EXIT_FAILURE);
  }


  if (child_id == 0) { // sama dengan 0, berarti ini adalah proses child.
    pid_t child_id1; //  Mendeklarasikan variabel child_id1


    child_id1 = fork(); // Membuat proses child kedua dengan menggunakan fungsi fork().


    if (child_id1 < 0) {
      exit(EXIT_FAILURE);
    }


    if (child_id1 == 0) {
      char *argv[] = {"wget", "-O", "inifolderetc.zip", "drive.google.com/u/3/uc?id=1xBFLFSOsN-DuQCAkKWxyk4GlFH8SprlT&export=download&confirm=yes", NULL};
      execv("/bin/wget", argv);
      // ama dengan 0, berarti ini adalah proses child kedua yang akan menjalankan perintah
      // wget untuk mengunduh file dari URL tertentu menggunakan execv().
    }
    else{
      wait(NULL);
      char *argv[] = {"unzip", "inifolderetc.zip", NULL};
      execv("/usr/bin/unzip", argv);
      // Jika bukan kedua kondisi di atas, proses ini adalah proses parent pertama
      // yang akan menunggu proses child kedua selesai.
    }


   
  } else { // Pada proses parent pertama:


    wait(NULL); // Menunggu proses child pertama selesai.


    umask(0);
    char cwd[1000];
    // Mendeklarasikan array cwd untuk menyimpan current working directory (lokasi file saat ini).
    getcwd(cwd, sizeof(cwd));
    // Mendapatkan current working directory dan menyimpannya dalam array cwd.
    snprintf(SOURCE_DIR, sizeof(SOURCE_DIR), "%s/inifolderetc/sisop", cwd);
    /*
    Mengatur nilai SOURCE_DIR dengan memformat string menggunakan snprintf().
    Nilai ini akan digunakan sebagai direktori sumber (source directory) untuk file sistem
    yang akan di-mount menggunakan FUSE.
    */
    snprintf(MOUNT_POINT, sizeof(MOUNT_POINT), "%s/mod", cwd);
    /*
    Mengatur nilai MOUNT_POINT dengan memformat string menggunakan snprintf().
    Nilai ini akan digunakan sebagai titik mount (mount point)
    untuk file sistem yang akan di-mount menggunakan FUSE.
    */


    return fuse_main(argc, argv, &xmp_oper, NULL);
    // memanggil fungsi fuse_main() untuk menjalankan file sistem yang di-mount
    // menggunakan FUSE dengan menggunakan fungsi-fungsi operasi yang telah
    // ditentukan dalam struktur xmp_oper.
  }
}



