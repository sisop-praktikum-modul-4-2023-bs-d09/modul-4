#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <libgen.h>

//Deklarasi variabel log_file yang bertipe pointer ke FILE
FILE *log_file;

//menutup file log ketika program berakhir
void closeLogFile()
{
    //memeriksa apakah log_file tidak bernilai NULL
    if (log_file != NULL)
    {
	//jika iya maka akan menutup file dan mengatur log_file menjadi NULL
        fclose(log_file);
        log_file = NULL;
    }
}

// Menulis entri log ke file txt
void write_log(const char *status, const char *command, const char *description)
{
    //ambil waktu saat ini kemudian simpan dalam timestamp
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", tm);

    //fungsi membuka file log dengan mode a (append)
    FILE *log_file = fopen("logmucatatsini.txt", "a");
    if (log_file)
    {
	//tulis entri log kedalam file
        fprintf(log_file, "%s::%s::%s::%s-%s\n", status, timestamp, command, getlogin(), description);
	//tutup file log
        fclose(log_file);
    }
}

// Mendapatkan atribut dari file/direktori
// stbuf (pointer ke struct stat yang akan diisi atribut)
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;

    //dapatkan atribut dari path yang diberi
    res = lstat(path, stbuf);

    //jika ada kesalahan maka mengembalikan nilai -errno
    if (res == -1)
        return -errno;
    //jika tidak ada kesalahan
    return 0;
}

// Membaca isi direktori
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    //membuka direktori
    dp = opendir(path);

    //jika terdapat kesalahan
    if (dp == NULL)
        return -errno;

    //membaca setiap entri direktori
    while ((de = readdir(dp)) != NULL)
    {
	//mengisi info entri kedalam struct stat menggunakan filler
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

	//jika filler mengembalikan nilai non-zero pembacaan dihentikan
        if (filler(buf, de->d_name, &st, 0))
            break;
    }
    closedir(dp);
    return 0;
}

// Membaca data dari suatu file yang terbuka
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;
    (void)fi;

    //buka file dengan path yang diberikan
    fd = open(path, O_RDONLY);

    //jika mengembalikan nilai -1 maka ada kesalahan
    if (fd == -1)
        return -errno;

    //baca data dari file menggunakan pread()
    res = pread(fd, buf, size, offset);

    //jika menunjukkan adanya suatu kesalahan
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}

// Membuat direktori baru
static int xmp_mkdir(const char *path, mode_t mode)
{
    // ambil path dan simpan dalam variabel desc
    char desc[1000];
    sprintf(desc, "Create directory %s.", path);

    //jika path mengandung kata "restricted"
    if (strstr(path, "restricted"))
    {
	//fungsi akan menulis FAILED dan mengembalikan -EPERM
        write_log("FAILED", "MKDIR", desc);
	//menunjukkan adanya kesalahan izin
        return -EPERM;
    }

    int res;
    res = mkdir(path, mode);
    if (res == -1)
    {
        write_log("FAILED", "MKDIR", strerror(errno));
        return -errno;
    }

    //jika tidak ada kesalahan maka tulis log "SUCCES"
    write_log("SUCCESS", "MKDIR", desc);

    return 0;
}

// Mengubah nama file atau direktori
static int xmp_rename(const char *from, const char *to)
{
    // ambil from dan to kemudian simpan dalam variabel desc
    char desc[1000];
    sprintf(desc, "Rename from %s to %s.", from, to);

    //variabel bypass untuk mengecek form mengandung kata"bypass"
    char bypass[1000];
    sprintf(bypass, "%s", strstr(from, "bypass")); //strstr untuk memeriksa pengizinan rename

    //jika operasi rename tidak diizinkan
    if (((strstr(from, "restricted") && !strstr(from, "bypass") && (!strstr(to, "bypass"))) || (strstr(bypass, "restricted"))))
    {
	//tulis FAILED
        write_log("FAILED", "RENAME", desc);
        return -EPERM;
    }

    //jika tidak ada kesalahan izin
    int res = rename(from, to);
    if (res == -1)
        return -errno;

    //fungsi akan menuliskan SUCCES
    write_log("SUCCESS", "RENAME", desc);
    return 0;
}

// Menghapus file
static int xmp_unlink(const char *path)
{
    // ambil path dan simpan dalam desc
    char desc[1000];
    sprintf(desc, "Remove file %s.", path);

    //jika path mengandung kata "restricted"
    if (strstr(path, "restricted") && !strstr(path, "bypass"))
    {
	//fungsi akan menulis log FAILED
        write_log("FAILED", "UNLINK", desc);
        return -EPERM;
    }

    //jika tidak ada kesalahan izin
    int res = unlink(path); //unlink untuk menghapus file
    if (res == -1)
        return -errno;

    write_log("SUCCESS", "UNLINK", desc);
    return 0;
}

// Menghapus direktori
static int xmp_rmdir(const char *path)
{
    // ambil path dan simpan dalam desc
    char desc[1000];
    sprintf(desc, "Remove directory %s.", path);

    //jika mengandung kata "restricted"
    if (strstr(path, "restricted") && !strstr(path, "bypass"))
    {
	//tulis log "FAILED"
        write_log("FAILED", "RMDIR", desc);
        return -EPERM;
    }

    int res = rmdir(path);
    if (res == -1)
        return -errno;

    write_log("SUCCESS", "RMDIR", desc);
    return 0;
}

//pointer ke fungsi yang akan digunakan dalam mengimplementasikan operasi
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,
};

int main(int argc, char *argv[])
{
    //buka file txt dalam mode w (tulis) dan simpan dalam log_file
    log_file = fopen("logmucatatsini.txt", "w");
    //menutup file log sebelum program berakhir
    atexit(closeLogFile);
    //mengizinkan hak akses penuh saat membuat file atau direktori
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, "nanaxgerma/src_data/");
}
