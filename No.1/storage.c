#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define LENGTH 512

void alter (){
    // system ("cd Downloads");
    system ("unzip -n archive.zip");
}

void PART_A(){
    system ("kaggle datasets download -d bryanb/fifa-player-stats-database");

    system ("unzip -n fifa-player-stats-database.zip");
}

void PART_B(){
    FILE* file = fopen ("./FIFA23_official_data.csv", "r"); //open file 
    char line[LENGTH];
    fgets(line, sizeof(line), file);

    while(fgets(line, sizeof(line), file) != NULL){
        char ID[LENGTH];
        char Name[LENGTH];
        int Age;
        char Club[LENGTH];
        char Nationality[LENGTH];
        int potential;
        char Photo_link[LENGTH];
        int Overall;
        char Flag_link[LENGTH];
        int Potential;
        char Clublogo[LENGTH];
        // em, link  ight?

        // id>name>age>photo>nation>flag>overall>potential>club>clublogo
        char* dataPart = strtok(line, ",");//partial
        strcpy(ID,dataPart); 
        // memecah string menjadi serangkaian dataPart (tergantung pembatas coy)

        dataPart = strtok(NULL, ",");
            strcpy(Name, dataPart);

        dataPart = strtok(NULL, ",");
            Age = atoi(dataPart);
            // atoi string to int

        dataPart = strtok(NULL, ",");
            strcpy(Photo_link, dataPart);

        dataPart = strtok(NULL, ",");
            strcpy(Nationality, dataPart);

        dataPart = strtok(NULL, ",");
            strcpy(Flag_link, dataPart);

        dataPart = strtok(NULL, ",");
            Overall = atoi(dataPart);

        dataPart = strtok(NULL, ",");
            Potential = atoi(dataPart);

        dataPart = strtok(NULL, ",");
            strcpy(Club, dataPart);

        dataPart = strtok(NULL, ",");
            strcpy(Clublogo, dataPart);


    if (Potential <= 85 || Age >=25) {
      continue;
    }

    printf("Name\t\t: %s\n", Name);
    printf("Age\t\t: %d\n", Age);
    printf("Club\t\t: %s\n", Club);
    printf("Nationality\t: %s\n", Nationality);
    printf("Potential\t: %d\n", Potential);
    printf("Photo_link\t: %s\n", Photo_link);
    printf("\n");
    }

    fclose(file);

}

int main(){

        PART_A();
        // alter();
        PART_B();
}