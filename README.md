# Modul 4

### Kelompok D09
| Nama | NRP |
| ------ | ------ |
| Christian Kevin Emor | 5025211153 |
| Andrian Tambunan | 5025211018 |
| Helmi Abiyu Mahendra | 5025211061 |


### Problem 1
##### Deskripsi Problem:
Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.

#### Point 1a
##### Deskripsi Problem:
Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.
```sh
kaggle datasets download -d bryanb/fifa-player-stats-database
```
Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 
##### Solusi
Untuk mendownload set data dari kaggle, perlu melakukan beberapa prosedur:
install python3-pip> install kaggle> copy kaggle.json ke directory .kaggle

untuk mendapatkan file kaggle.json, ditemukan pada:
```
https://www.kaggle.com/settings/account
```
*pastikan sudah memiliki akun terlebih dahulu
scroll down sampai bagian API, dan pilih "CREATE NEW API"

setelah itu, barulah dataset dapat didownload dalam bentuk .zip, yang kemudian bisa diextract
##### Potongan Code
```sh
//via terminal linux ubuntu
//install kaggle
pip install --user kaggle

/*buat direktori .kaggle pada root, dan pindahkan kaggle.json ke direktory tsb*/
mkdir ~/.kaggle
mv kaggle.json ~/.kaggle

/*ubah permission kaggle.json agar tidak dibaca user lain*/
chmod 600 ~/.kaggle/kaggle.json

//download dan extract dataset
void PART_A(){
    system ("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system ("unzip fifa-player-stats-database.zip");
}
```
#### Point 1b
##### Deskripsi Problem
Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.
##### Solusi
hasil ekstraksi dataset berisi file csv yang bisa dibuka dengan fopen. Isi dari file csv dapat diubah menjadi data terpisah dengan tokenisasi.
##### Code
```sh
FILE* file = fopen ("./FIFA23_official_data.csv", "r");
//buka file FIFA23_official_data.csv
    char line[LENGTH];
    fgets(line, sizeof(line), file);
    //ambil string dari setiap line

    while(fgets(line, sizeof(line), file) != NULL){
        char ID[LENGTH];
        char Name[LENGTH];
        int Age;
        char Club[LENGTH];
        char Nationality[LENGTH];
        int potential;
        char Photo_link[LENGTH];
        int Overall;
        char Flag_link[LENGTH];
        int Potential;
        char Clublogo[LENGTH];
        // em, link  ight?

        // id>name>age>photo>nation>flag>overall>potential>club>clublogo
        // urutan data pada file
        
        
        char* dataPart = strtok(line, ",");//partial
        strcpy(ID,dataPart); 
        // memecah string menjadi serangkaian dataPart (tergantung pembatas)

        dataPart = strtok(NULL, ",");
            strcpy(Name, dataPart);

        dataPart = strtok(NULL, ",");
            Age = atoi(dataPart);
            // atoi string to int

        dataPart = strtok(NULL, ",");
            strcpy(Photo_link, dataPart);

        dataPart = strtok(NULL, ",");
            strcpy(Nationality, dataPart);

        dataPart = strtok(NULL, ",");
            strcpy(Flag_link, dataPart);

        dataPart = strtok(NULL, ",");
            Overall = atoi(dataPart);

        dataPart = strtok(NULL, ",");
            Potential = atoi(dataPart);

        dataPart = strtok(NULL, ",");
            strcpy(Club, dataPart);

        dataPart = strtok(NULL, ",");
            strcpy(Clublogo, dataPart);


    if (Potential <= 85 || Age >=25) {
      continue;
    }
    //constraits pemain 
    
    //print data pemain yang sesuai persyaratan
    printf("Name\t: %s\n", Name);
    printf("Age\t: %d\n", Age);
    printf("Club\t: %s\n", Club);
    printf("Nationality\t: %s\n", Nationality);
    printf("Potential\t: %d\n", Potential);
    printf("Photo_link\t: %s\n", Photo_link);
    printf("\n");
    }

    fclose(file);
//tutup file
```
##### Penjelasan


#### Point 1c
##### Deskripsi Problem
Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.
##### Solusi
Buat docker container.
dockerfile>docker image> docker container
##### Code
```sh
#pada terminal linux
#masuk ke direktori dg file storage
cd [dir]

docker build . (on directory)
docker build --tag [nama]:[versi] .
#exp : docker build --tag storage-app:1.0

#cek keberadaan docker images
docker images

#buat docker container
docker container create --name [namacontainer] -p [port]:[port] [namaimage]
#exp docker container create --name strgapp -p 8080:8080 storage-app:1.0

```
#### Point 1d
##### Deskripsi Problem
karena kesuksesan program, maka perlu diupload ke dockerhub agar dapat digunakan pihak lain
##### Solusi
buat akun dockerhub, dan push docker image pada repository
##### Code
```sh
#terminal
#login docker
docker login

docker tag imageid username/repository
#exp :docker tag a91e9b07e0e8 envytia/storage_app

docker push username/repository
#exp :docker push envytia/storage_app
```
#### Point 1e
##### Deskripsi Problem
seiring dengan permintaan yg terus bertambah, diputuskan untuk membuat layanan dg docker compose dengan 5 instance. Buat folder Barcelona dan Napoli, serta jalankan docker compose di sana
##### Solusi
##### Code
```sh
```

### Problem No 2
##### Deskripsi Problem:
Nana adalah peri kucing yang terampil dalam menggunakan sihir, dia bisa membuat orang lain berubah menjadi Molina. Suatu hari, dia bosan menggunakan sihir dan mencoba untuk magang di germa.dev sebagai programmer.
Agar dapat diterima sebagai karyawan magang dia diberi sebuah file .zip yang berisi folder dan file dari Germa. Kemudian, Nana harus membuat sistem manajemen folder. Setelah Nana paham aturan tersebut, bantu Nana untuk membuat sebuah FUSE yang bernama germa.c, yang mana dapat melakukan make folder, rename file dan folder, serta delete file dan folder. Semua kegiatan yang kalian lakukan harus tercatat dalam sebuah file logmucatatsini.txt di luar file .zip yang diberikan kepada Nana, agar pekerjaan yang kamu lakukan dapat dilaporkan kepada Germa. 

##### Solusi
1. Deklarasi variabel log_file yang bertipe pointer ke FILE

##### Potongan Code
```
FILE *log_file;

//menutup file log ketika program berakhir
void closeLogFile()
{
    //memeriksa apakah log_file tidak bernilai NULL
    if (log_file != NULL)
    {
	//jika iya maka akan menutup file dan mengatur log_file menjadi NULL
        fclose(log_file);
        log_file = NULL;
    }
}
```
2. Menulis entri log ke file txt

##### Potongan Code
```
void write_log(const char *status, const char *command, const char *description)
{
    //ambil waktu saat ini kemudian simpan dalam timestamp
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", tm);

    //fungsi membuka file log dengan mode a (append)
    FILE *log_file = fopen("logmucatatsini.txt", "a");
    if (log_file)
    {
	//tulis entri log kedalam file
        fprintf(log_file, "%s::%s::%s::%s-%s\n", status, timestamp, command, getlogin(), description);
	//tutup file log
        fclose(log_file);
    }
}
```
3. Mendapatkan atribut dari file/direktori

##### Potongan Code
```
// stbuf (pointer ke struct stat yang akan diisi atribut)
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;

    //dapatkan atribut dari path yang diberi
    res = lstat(path, stbuf);

    //jika ada kesalahan maka mengembalikan nilai -errno
    if (res == -1)
        return -errno;
    //jika tidak ada kesalahan
    return 0;
}

```
4. Membaca isi direktori

##### Potongan Code
```
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    //membuka direktori
    dp = opendir(path);

    //jika terdapat kesalahan
    if (dp == NULL)
        return -errno;

    //membaca setiap entri direktori
    while ((de = readdir(dp)) != NULL)
    {
	//mengisi info entri kedalam struct stat menggunakan filler
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

	//jika filler mengembalikan nilai non-zero pembacaan dihentikan
        if (filler(buf, de->d_name, &st, 0))
            break;
    }
    closedir(dp);
    return 0;
}


```
5. Membaca data dari suatu file yang terbuka

##### Potongan Code
```
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;
    (void)fi;

    //buka file dengan path yang diberikan
    fd = open(path, O_RDONLY);

    //jika mengembalikan nilai -1 maka ada kesalahan
    if (fd == -1)
        return -errno;

    //baca data dari file menggunakan pread()
    res = pread(fd, buf, size, offset);

    //jika menunjukkan adanya suatu kesalahan
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}
```
6. Membuat direktori baru

##### Potongan Code
```
static int xmp_mkdir(const char *path, mode_t mode)
{
    // ambil path dan simpan dalam variabel desc
    char desc[1000];
    sprintf(desc, "Create directory %s.", path);

    //jika path mengandung kata "restricted"
    if (strstr(path, "restricted"))
    {
	//fungsi akan menulis FAILED dan mengembalikan -EPERM
        write_log("FAILED", "MKDIR", desc);
	//menunjukkan adanya kesalahan izin
        return -EPERM;
    }

    int res;
    res = mkdir(path, mode);
    if (res == -1)
    {
        write_log("FAILED", "MKDIR", strerror(errno));
        return -errno;
    }

    //jika tidak ada kesalahan maka tulis log "SUCCES"
    write_log("SUCCESS", "MKDIR", desc);

    return 0;
}
```

7. Mengubah nama file atau direktori

##### Potongan Code
```
static int xmp_rename(const char *from, const char *to)
{
    // ambil from dan to kemudian simpan dalam variabel desc
    char desc[1000];
    sprintf(desc, "Rename from %s to %s.", from, to);

    //variabel bypass untuk mengecek form mengandung kata"bypass"
    char bypass[1000];
    sprintf(bypass, "%s", strstr(from, "bypass")); //strstr untuk memeriksa pengizinan rename

    //jika operasi rename tidak diizinkan
    if (((strstr(from, "restricted") && !strstr(from, "bypass") && (!strstr(to, "bypass"))) || (strstr(bypass, "restricted"))))
    {
	//tulis FAILED
        write_log("FAILED", "RENAME", desc);
        return -EPERM;
    }

    //jika tidak ada kesalahan izin
    int res = rename(from, to);
    if (res == -1)
        return -errno;

    //fungsi akan menuliskan SUCCES
    write_log("SUCCESS", "RENAME", desc);
    return 0;
}
```

8. Menghapus file

##### Potongan Code
```
static int xmp_unlink(const char *path)
{
    // ambil path dan simpan dalam desc
    char desc[1000];
    sprintf(desc, "Remove file %s.", path);

    //jika path mengandung kata "restricted"
    if (strstr(path, "restricted") && !strstr(path, "bypass"))
    {
	//fungsi akan menulis log FAILED
        write_log("FAILED", "UNLINK", desc);
        return -EPERM;
    }

    //jika tidak ada kesalahan izin
    int res = unlink(path); //unlink untuk menghapus file
    if (res == -1)
        return -errno;

    write_log("SUCCESS", "UNLINK", desc);
    return 0;
}
```

9. Menghapus direktori

##### Potongan Code
```
static int xmp_rmdir(const char *path)
{
    // ambil path dan simpan dalam desc
    char desc[1000];
    sprintf(desc, "Remove directory %s.", path);

    //jika mengandung kata "restricted"
    if (strstr(path, "restricted") && !strstr(path, "bypass"))
    {
	//tulis log "FAILED"
        write_log("FAILED", "RMDIR", desc);
        return -EPERM;
    }

    int res = rmdir(path);
    if (res == -1)
        return -errno;

    write_log("SUCCESS", "RMDIR", desc);
    return 0;
}
```

10. Fungsi Main

##### Potongan Code
```
int main(int argc, char *argv[])
{
    //buka file txt dalam mode w (tulis) dan simpan dalam log_file
    log_file = fopen("logmucatatsini.txt", "w");
    //menutup file log sebelum program berakhir
    atexit(closeLogFile);
    //mengizinkan hak akses penuh saat membuat file atau direktori
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, "nanaxgerma/src_data/");
}
```

###### Buatlah folder productMagang pada folder /src_data/germa/products/restricted_list/. Kemudian, buatlah folder projectMagang pada /src_data/germa/projects/restricted_list/. Akan tetapi, hal tersebut akan gagal.

kegagalan disebabkan adanya "restricted" didalam penamaan

```
mkdir productMagang
mkdir projectMagang
```

###### Ubahlah nama folder dari restricted_list pada folder /src_data/germa/projects/restricted_list/ menjadi /src_data/germa/projects/bypass_list/. Kemudian, buat folder projectMagang di dalamnya.

```
mv restricted_list bypass_list
mkdri projectMagang
```

###### Karena folder projects menjadi dapat diakses, ubahlah folder filePenting di dalam folder projects menjadi restrictedFilePenting agar secure kembali. Coba keamanannya dengan mengubah nama file yang ada di dalamnya.

```
mv ourProject.txt weProject.txt
```

###### Terakhir, coba kamu hapus semua fileLama di dalam folder restrictedFileLama. Jangan lupa untuk menambahkan kata sihir pada folder tersebut agar folder tersebut dapat terhapus

folder dapat dihapus walaupun mengandung kalimat "restricted" karena mengandung kata-kata sihir yang dimiliki Nana :D

```
mv restrictedFileLama restrictedFileLamabypass
rm -r restrictedFileLamabypass
```
### Problem 3
##### Deskripsi Problem:
Dhafin adalah seorang yang sangat pemalu, tetapi saat ini dia sedang mengagumi seseorang yang dia tidak ingin seorangpun tahu. Maka dari itu, dia melakukan berbagai hal sebagai berikut.

#### Point 3a
##### Deskripsi Problem:
Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah /etc yang bernama secretadmirer.c. 
##### Solusi
membuat file secretadmire.c dan Mounting fuse dengan code sebagai berikut
##### Code 
```
gcc -Wall secretadmire.c `pkg-config fuse --cflags --libs` -o secretadmire
```
#### Point 3b & 3c
##### Deskripsi Problem
Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64.  Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).

Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.
##### Solusi
ika karakter tersebut adalah 'L', 'U', 'T', atau 'H' baik dalam huruf besar maupun huruf kecil.
Hal ini dilakukan dengan menggunakan fungsi base64_encode yang tidak terlihat dalam kode yang diberikan. Setelah itu, nama filter_name dicetak dengan menggunakan printf.
Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.
##### Code Base64
```
static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";


char* base64_encode(const unsigned char* data, size_t inputLen) {
    // Fungsi ini digunakan untuk mengenkripsi string menggunakan algoritma base64.


    // Fungsi ini menerima argumen data (string yang akan dienkripsi)
    // dan inputLen (panjang data) dan mengembalikan hasil enkripsi.
    printf("Former Name: %s\n", data);
    size_t output_length = 4 * ((inputLen + 2) / 3);
    char* hasilEncode = (char*)malloc(output_length + 1);


    size_t i, j;
    for (i = 0, j = 0; i < inputLen; i += 3, j += 4) {
        unsigned char octet_a = i < inputLen ? data[i] : 0;
        unsigned char octet_b = i + 1 < inputLen ? data[i + 1] : 0;
        unsigned char octet_c = i + 2 < inputLen ? data[i + 2] : 0;


        hasilEncode[j] = base64_table[octet_a >> 2];
        hasilEncode[j + 1] = base64_table[((octet_a & 3) << 4) | (octet_b >> 4)];
        hasilEncode[j + 2] = base64_table[((octet_b & 15) << 2) | (octet_c >> 6)];
        hasilEncode[j + 3] = base64_table[octet_c & 63];
    }


    if (inputLen % 3 == 1) {
        hasilEncode[output_length - 2] = '=';
        hasilEncode[output_length - 1] = '=';
    } else if (inputLen % 3 == 2) {
        hasilEncode[output_length - 1] = '=';
    }


    hasilEncode[output_length] = '\0';
    printf("Encoded Name: %s\n", hasilEncode);
    return hasilEncode;
}
```
##### Code Filter L, U, T, H
```
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    // untuk membaca isi dari sebuah direktori yang diidentifikasi oleh path.
    DIR *dp;
    struct dirent *de;
    /*
    Mendeklarasikan pointer dp untuk menunjukkan ke objek direktori
    yang akan dibaca, dan pointer de untuk menyimpan entri direktori
    saat ini selama iterasi.
    */
    char fpath[MAX_PATH]; // Mendeklarasikan array karakter fpath dengan ukuran MAX_PATH
    snprintf(fpath, sizeof(fpath), "%s%s", SOURCE_DIR, path); // untuk menggabungkan SOURCE_DIR (direktori sumber) dengan path yang diberikan ke dalam fpath.
    (void) offset;
    (void) fi;
    // Mengabaikan parameter offset dan fi untuk menghilangkan peringatan "unused variable" saat kompilasi.
    dp = opendir(fpath);
    if (dp == NULL) return -errno; // Jika operasi ini gagal (dp == NULL), maka fungsi akan mengembalikan nilai negatif dari errno.


    while ((de = readdir(dp)) != NULL) {
        //Memulai loop while untuk membaca entri direktori satu per satu menggunakan fungsi readdir hingga tidak ada entri lagi.
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
            continue;
            // Melakukan pengecekan apakah nama entri saat ini adalah "."
            // (direktori itu sendiri) atau ".." (direktori induk).
            // Jika ya, maka loop akan melanjutkan ke entri direktori berikutnya.


        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;


        char filter_name[MAX_PATH];
        memset(filter_name, 0, sizeof(filter_name));


        strcpy(filter_name, de->d_name);


        // kondisi LUTH
        // jika karakter tersebut adalah 'L', 'U', 'T', atau 'H' baik dalam huruf besar maupun huruf kecil,
        // maka nama filter_name akan diubah
        if (filter_name[0] == 'L' || filter_name[0] == 'l' ||
            filter_name[0] == 'U' || filter_name[0] == 'u' ||
            filter_name[0] == 'T' || filter_name[0] == 't' ||
            filter_name[0] == 'H' || filter_name[0] == 'h') {
           
            char* encoded = base64_encode((unsigned char*)filter_name, strlen(filter_name));
            snprintf(filter_name, sizeof(filter_name), "%s", encoded);
            printf("Convert Name: %s\n", filter_name);
            free(encoded);
        }
        /*
        Hal ini dilakukan dengan menggunakan fungsi base64_encode yang tidak terlihat
        dalam kode yang diberikan. Setelah itu, nama filter_name dicetak dengan
        menggunakan printf.
        */


        // kondisi < 5 ch
        if (strlen(filter_name) < 5) {
            printf("Less than 5: %s\n", filter_name);
            // maka setiap karakter dalam filter_name akan dikonversi menjadi representasi biner dengan 8 bit.


            char new_conv[MAX_PATH];
            memset(new_conv, 0, sizeof(new_conv));
            // Karakter-karakter biner ini akan digabungkan ke dalam new_conv, dan
            // filter_name akan diubah menjadi isi dari new_conv yang baru.
            // Setelah itu, nama filter_name dicetak dengan menggunakan printf.
            for (size_t i = 0; i < strlen(filter_name); i++) {
                char binary[10];
                binary[8] = ' ';
                binary[9] = '\0';


                unsigned char ch = filter_name[i];
                for (int j = 7; j >= 0; j--) {
                    binary[j] = (ch & 1) + '0';
                    ch >>= 1;
                }


                strcat(new_conv, binary);
            }
            memset(filter_name, 0, sizeof(filter_name));
            strcpy(filter_name, new_conv);
            printf("After convert 5: %s\n\n", filter_name);
        }


        if (S_ISREG(st.st_mode)) {
            //Memeriksa apakah entri saat ini adalah sebuah file reguler atau direktori.
            for (int i = 0; filter_name[i]; i++) { // Jika file reguler
                filter_name[i] = tolower(filter_name[i]);
                // maka nama filter_name akan diubah menjadi huruf kecil menggunakan loop for dan fungsi tolower().
            }


            char old_dir_path[MAX_PATH*2];
            snprintf(old_dir_path, sizeof(old_dir_path), "%s/%s", fpath, de->d_name);


            char new_dir_path[MAX_PATH*2];
            snprintf(new_dir_path, sizeof(new_dir_path), "%s/%s", fpath, filter_name);
            // dibentuk path absolut lama (old_dir_path) dan path absolut baru (new_dir_path) menggunakan snprintf
            rename(old_dir_path, new_dir_path);
            // entri direktori dengan path lama akan diubah menjadi path baru dengan nama yang telah diubah.
        }
        else{
            /*
            Jika entri saat ini adalah direktori, maka nama filter_name akan diubah
            menjadi huruf besar menggunakan loop for dan fungsi toupper().
            Proses rename yang sama juga dilakukan untuk direktori.
            */
            for (int i = 0; filter_name[i]; i++) {
                filter_name[i] = toupper(filter_name[i]);
            }


            char old_dir_path[MAX_PATH*2];
            snprintf(old_dir_path, sizeof(old_dir_path), "%s/%s", fpath, de->d_name);


            char new_dir_path[MAX_PATH*2];
            snprintf(new_dir_path, sizeof(new_dir_path), "%s/%s", fpath, filter_name);


            rename(old_dir_path, new_dir_path);
        }


        if (S_ISDIR(st.st_mode)) {


            char new_path[MAX_PATH*2];
            snprintf(new_path, sizeof(new_path), "%s/%s", path, filter_name);
            xmp_readdir(new_path, buf, filler, offset, fi); // pemanggilan rekursif ke fungsi xmp_readdir
        }


        filler(buf, filter_name, &st, 0);
        /*
        filter_name merupakan nama entri yang telah diubah,
        &st adalah pointer ke struktur st yang berisi atribut entri tersebut, dan
        0 adalah offset.
        */
    }


    closedir(dp); // Menutup direktori
    return 0;
}
```
Berikutnya terdapat fungsi encode base64 yang akan merubah isi file menjadi terencode base64. Konten sebelum dan sesudah encode akan diprint pada terminal.
```
static  int  xmp_getattr(const char *path, struct stat *stbuf){
    int res;
    char fpath[MAX_PATH*2];

    sprintf(fpath,"%s%s",SOURCE_DIR,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}
```
#### Point 3d
##### Deskripsi Problem
Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan a.k.a terserah).
##### Code
```
#define MAX_PASSWORD_LENGTH 100 // Konstanta variabel.
char PASSWORD[MAX_PASSWORD_LENGTH] = "mypass";
```
```
static int xmp_open(const char *path, struct fuse_file_info *fi) {
    (void) fi;

    if (strcmp(path, "/") == 0) {
        path = SOURCE_DIR;
    } else {
        char fpath[MAX_PATH*2];
        sprintf(fpath, "%s%s", SOURCE_DIR, path);

        struct stat st;
        if (lstat(fpath, &st) == 0 && S_ISREG(st.st_mode)) {
            char inPW[100];
            printf("Enter your Password: ");
            fflush(stdout);
            fgets(inPW, sizeof(inPW), stdin);

            int len = strlen(inPW);
            if (len > 0) inPW[len - 1] = '\0';

            while (strcmp(inPW, PASSWORD) != 0) {
                printf("The password you just entered is incorrect! Please try again.\n\nEnter your Password: ");
                fflush(stdout);
                fgets(inPW, sizeof(inPW), stdin);

                len = strlen(inPW);
                if (len > 0) inPW[len - 1] = '\0';
            }
        }
    }

    return 0;
}
```
#### Problem 3e
##### Deskripsi Problem
Selanjutnya, Dhafin ingin melakukan mounting FUSE tersebut di dalam Docker Container menggunakan Docker Compose (gunakan base image Ubuntu Bionic ). Ketentuannya, terdapat dua buah container sebagai berikut.
Container bernama Dhafin akan melakukan mount FUSE yang telah dimodifikasi tersebut. 
Container bernama Normal akan melakukan mount yang hanya menampilkan /etc yang normal tanpa ada modifikasi apapun. 
##### Solusi
Mounting FUSE kedalam Docker dan menampilkan container Dhafin dan Normal.
#### Code Munting FUSE
```
gcc -Wall secretadmirer.c `pkg-config fuse --cflags --libs` -o secretadmirer
```
#### Code docker-compose.yml
```
version: "3"

services:
  dhafin:
    build:
      context: .
      dockerfile: Dockerfile-dhafin
    container_name: Dhafin #nama container 1
    volumes:
      - /home/praktikum/modul4/dockerTest:/etc/dhafin # nempatin /etc/dhafin di directory sebelah kiri ':' 
    command: tail -f /dev/null

  normal:
    image: ubuntu:bionic 
    container_name: Normal # Nama container 2
    volumes:
      - /home/praktikum/modul4/dockerTest:/etc/normal # nempatin /etc/normal di directory sebelah kiri ':' 
    command: tail -f /dev/null

```
Berikut merupakan file docker-compose yang digunakan untuk membuat docker container Dhafin dan normal. Pada container Dhafin menggunakan dockerfile khusus untuk install library dan run program yang dibutuhkan. 
Pada masing-masing container terdapat command tail -f /dev/null yang menjadikan container tidak exit walau program berhasil dieksekusi.
